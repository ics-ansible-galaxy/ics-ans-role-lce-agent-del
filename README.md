# ics-ans-role-lce-agent-del

Ansible role to install lce-agent-del.

## Role Variables

```yaml
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-lce-agent-del
```

## License

BSD 2-clause
